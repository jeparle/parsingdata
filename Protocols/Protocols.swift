//
//  Protocols.swift
//  Clima
//
//  Created by Leandro Ramos on 2/24/20.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation

protocol WeatherManagerDelegate {//delegate protocol
    func didUpdateWeather(_ weatherManager: WeatherManager,weather: WeaderModel)
    func didFailWithError(error: Error)
}
