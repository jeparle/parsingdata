//
//  WeatherManager.swift
//  Clima
//
//  Created by Leandro Ramos on 2/24/20.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation

struct WeatherManager {
    let weatherURL = "https://api.openweathermap.org/data/2.5/weather?"
    let celcius = "metric"
    let apiKey = "b6e8ba6abafc13b1b70985f9ce4bc9cf"
    
    var delegate: WeatherManagerDelegate?//declared vaiable delegate
    
    func fetchWeather(cityName: String) {
        let urlString = "\(weatherURL)q=\(cityName)&units=\(celcius)&appid=\(apiKey)"
        performRequest(with: urlString)
    }
    
    func performRequest(with urlString: String) {
        //1. create a url
        
        if let url = URL(string: urlString) {
            
            //2. create URLSession
            
            let session = URLSession(configuration: .default)
            
            //3. Give the session Task
            
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    self.delegate?.didFailWithError(error: error!)
                    return
                }
                //utf8 data format
                if let safeData = data {
//                    let dataString = String(data: safeData, encoding: .utf8) //utf8 data format
                    if let weather = self.parseJSON(safeData) {
                        self.delegate?.didUpdateWeather(self, weather: weather)// delegate creation
                    }
                }
            }
            //4. Start the task
            task.resume()
        }
    }
    //the completionhandler i can put inside a function
    
    func parseJSON(_ weatherData: Data) -> WeaderModel? {// for line 42 to work. - = weatherData
        let decoder = JSONDecoder()
        do {
            
            let decodedData = try decoder.decode(WeatherData.self, from: weatherData)
            let id = decodedData.weather[0].id
            let temp = decodedData.main.temp
            let city = decodedData.name
            
            let weather = WeaderModel(conditionId: id, cityName: city, temperature: temp)
            return weather
                        
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }    
}
